/*
Implement a SnapshotArray that supports the following interface:

SnapshotArray(int length) initializes an array-like data structure with the given length. Initially, each element equals 0.
void set(index, val) sets the element at the given index to be equal to val.
int snap() takes a snapshot of the array and returns the snap_id: the total number of times we called snap() minus 1.
int get(index, snap_id) returns the value at the given index, at the time we took the snapshot with the given snap_id
*/
class SnapshotArray {
  TreeMap<Integer, Integer>[] map;
  int snap_id;

  public SnapshotArray(int length) {
    map = new TreeMap[length];
    snap_id = 0;
  }
  
  public void set(int index, int val) {
    if (map[index] == null) map[index] = new TreeMap<>();

    map[index].put(snap_id, val);
  }
  
  public int snap() {
    return snap_id++;
  }
  
  public int get(int index, int snap_id) {
    if (map[index] == null) return 0;
    
    var entry = map[index].floorEntry(snap_id);

    return entry == null ? 0 : entry.getValue();
  }
}