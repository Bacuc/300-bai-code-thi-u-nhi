//Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
//The overall run time complexity should be O(log (m+n)). 
class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int m = nums1.length;
        int n = nums2.length;
        int k = m + n;
        int[] new_arr = new int[k];

        int i=0, j=0, l=0;

        while (i<=m && j<=n) {
            if (i == m) {
                while(j<n) new_arr[l++] = nums2[j++];
                break;
            } else if (j == n) {
                while (i<m) new_arr[l++] = nums1[i++];
                break;
            }

            if (nums1[i] < nums2[j]) {
                new_arr[l++] = nums1[i++];
            } else {
                new_arr[l++] = nums2[j++];
            }
        }

        if (k%2==0) return (float)(new_arr[k/2-1] + new_arr[k/2])/2;
        else return new_arr[k/2];
    }
}