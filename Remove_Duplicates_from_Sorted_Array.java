/*
Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place such that each unique element appears only once. 
The relative order of the elements should be kept the same. Then return the number of unique elements in nums.
Consider the number of unique elements of nums to be k, to get accepted, you need to do the following things:
Change the array nums such that the first k elements of nums contain the unique elements in the order they were present in nums initially. 
The remaining elements of nums are not important as well as the size of nums.
Return k
*/
class Solution {
    public int removeDuplicates(int[] nums) {
        
// Java program to remove duplicates
        // Return, if array is empty or
        // contains a single element
        int n = nums.length;
        if (n == 0 || n == 1)
            return n;
 
        int[] temp = new int[n];
 
        // Start traversing elements
        int j = 0;
        for (int i = 0; i < n - 1; i++)
             
            // If current element is not equal to next
            // element then store that current element
            if (nums[i] != nums[i + 1])
                temp[j++] = nums[i];
 
        // Store the last element as whether it is unique or
        // repeated, it hasn't stored previously
        temp[j++] = nums[n - 1];
 
        // Modify original array
        for (int i = 0; i < j; i++)
            nums[i] = temp[i];
 
        return j;
    
    }
}

