/*
There are n 1-indexed robots, each having a position on a line, health, and movement direction.
You are given 0-indexed integer arrays positions, healths, and a string directions (directions[i] is either 'L' for left or 'R' for right). All integers in positions are unique.
All robots start moving on the line simultaneously at the same speed in their given directions. If two robots ever share the same position while moving, they will collide.
If two robots collide, the robot with lower health is removed from the line, and the health of the other robot decreases by one. The surviving robot continues in the same direction it was going. If both robots have the same health, they are both removed from the line.
Your task is to determine the health of the robots that survive the collisions, in the same order that the robots were given, i.e. final heath of robot 1 (if survived), final health of robot 2 (if survived), and so on. If there are no survivors, return an empty array.
Return an array containing the health of the remaining robots (in the order they were given in the input), after no further collisions can occur.

Note: The positions may be unsorted.
*/
class Solution {
    class Pair{
        int p;
        int h;
        char d;
        int idx;
        Pair(int p,int h,char d,int i){
            this.p=p;
            this.h=h;
            this.d=d;
            this.idx=i;
        }
    }
    public List<Integer> survivedRobotsHealths(int[] positions, int[] healths, String directions) {
          int n=positions.length;
          List<Pair> list=new ArrayList<>();
        
          for(int i=0;i<n;i++){
              list.add(new Pair(positions[i],healths[i],directions.charAt(i),i));
          }
        
          Collections.sort(list,(a,b)->a.p-b.p);
        
          List<int[]> ans=new ArrayList<>();
          Stack<int[]> q=new Stack<>();
        
          for(int i=0;i<n;i++){
              Pair curr=list.get(i);
              if(curr.d=='R'){
                  q.add(new int[]{curr.h,curr.idx});
                  continue;
              }
              else{
                  boolean flag=false;
                  int health=curr.h;

                  //check q is empty
                  while(!q.isEmpty() && health!=0){
                       int top[]=q.pop();
                       int last=top[0];
                       int idx=top[1];
                      
                       if(last<health){
                           health-=1;
                       }else if(last==health){
                           health=0;
                           break; 
                       }
                       else{
                          last-=1;
                          health=0;
                          q.add(new int[]{last,idx});
                           
                       }
                  }
                  
                  if(health!=0)
                      ans.add(new int[]{health,curr.idx});
              }
          }
        
        while(!q.isEmpty()){
              ans.add(q.pop());
        }
        
        Collections.sort(ans,(a,b)->a[1]-b[1]);
        
        List<Integer> result=new ArrayList<>();
        
        for(int i=0;i<ans.size();i++){
            result.add(ans.get(i)[0]);
        }
        
        return result;
    }
}