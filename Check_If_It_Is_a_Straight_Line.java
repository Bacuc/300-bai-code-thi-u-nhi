// class Solution {
//     public boolean checkStraightLine(int[][] coordinates) {
//         int xMove =  coordinates[1][0] - coordinates[0][0];
//         int yMove =  coordinates[1][1] - coordinates[0][1];
//         for(int i = 0; i < coordinates.length; i++){
//             int x = coordinates[i][0] - coordinates[i-1][0];
//             int y = coordinates[i][1] - coordinates[i-1][1];


//             //
//             if(y * xMove != x *yMove)
//                 return false;
//         }
//         return true;
        
//     }
// }
class Solution {
public boolean checkStraightLine(int[][] c) {
        
        
        int xMove = c[1][0] - c[0][0];
        int yMove = c[1][1] - c[0][1];
        
        for(int i=1; i<c.length; i++){

            int x = c[i][0] - c[i-1][0];
            int y = c[i][1] - c[i-1][1];
            
// linear function -> y = k * x + b;
// Here need to discuss y = k * x;
// k = yMove / xMove;
// y * xMove = x * yMove;
            
            if(y * xMove != x * yMove) return false;
        }
        
        return true;
    }
}