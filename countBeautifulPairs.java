/*
Beautyfil pair
*/
class Solution {
    public int countBeautifulPairs(int[] nums) {
        String[] arr = new String[nums.length];
        for(int i =0; i < nums.length; i ++) arr[i] = Integer.toString(nums[i]);
        int ans = 0;
        for(int i =0; i< arr.length; i++){
            for(int j = i+1; j< arr.length; j++){
                String num1 = arr[i];
                String num2 = arr[j];
                
                if( gcd(num1.charAt(0) - '0', num2.charAt(num2.length() - 1) -'0') ==1){
                    ans++;
                }
            }
        }    
        return ans;
        
    }
    
    public int gcd(int a, int b)
    {
      if (b == 0)
        return a;
      return gcd(b, a % b);
    }
    
}