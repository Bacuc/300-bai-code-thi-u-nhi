
class Solution {
     public int[] getAverages(int[] nums, int k) {
         int[] avg = new int[nums.length];
         Arrays.fill(avg, -1);
         int width = k + k + 1;
         if (width <= nums.length) {
             long sum = 0;
             for (int i = width - 2; i >= 0; i--) {
                 sum += nums[i];
             }
             for (int i = width - 1; i < nums.length; i++) {
                 sum += nums[i];
                 avg[i - k] = (int) (sum / width);
                 sum -= nums[i + 1 - width];
             }
         }
         return avg;
     }
 }
 