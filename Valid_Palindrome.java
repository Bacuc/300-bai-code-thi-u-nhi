/*
A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters,
it reads the same forward and backward. Alphanumeric characters include letters and numbers.
Given a string s, return true if it is a palindrome, or false otherwise.
*/
class Solution {
    public boolean isPalindrome(String s) {
        String ans = "";
        s = s.toLowerCase();
        s = s.replace(" ","");
        s = s.replaceAll("\\p{Punct}","");

        for(int i = s.length()-1;i >= 0;i--){
            if(s.charAt(i)==(':')||s.charAt(i)==(',')){
               continue;
            }
            
            ans = s.charAt(s.length()-1-i)+ans;
            ans = ans.trim();

        }
        if(s.equals(ans)){
            return true;
        }
        return false;        
    }
}
